import React from "react";
import { Tr, Td } from "@chakra-ui/react";

import { DeleteIcon, EditIcon } from "@chakra-ui/icons";

export const OneRow = ({ employees, deleteEmployee }) => {
  return employees.map((employee) => (
    <Tr key={employee.name}>
      <Td>{employee.name}</Td>
      <Td>{employee.surname}</Td>
      <Td>{employee.email}</Td>
      <Td>{employee.fonction}</Td>
      <Td>{employee.qualif}</Td>
      <Td>{employee.niveau}</Td>
      <Td>{employee.valid}</Td>
      <Td>
        <DeleteIcon onClick={() => deleteEmployee(employee.id)} />
      </Td>
      <Td>
        <EditIcon onClick={() => deleteEmployee(employee.id)} />
      </Td>
    </Tr>
  ));
};
