import React from "react";
import towerimage from "./assets/towerimage.jpg";
import radar from "./assets/radar.jpg";
import NavBar from "../navbar/NavBar";
import {
  Spacer,
  Box,
  Flex,
  Heading,
  Text,
  Image,
  HStack,
} from "@chakra-ui/react";

function Home() {
  return (
    <>
      <NavBar />
      <HStack>
        <Flex minWidth="max-content" alignItems="center" gap="2">
          <Box maxW="500" textAlign="center">
            <Heading as="h1" size="4xl" fontSize={{ base: "7xl", md: "6xl" }}>
              Tour de contrôle
            </Heading>
            <Text
              mb="18"
              p="15"
              fontWeight="extrabold"
              size="6xl"
              bgGradient="linear(to-r, pink.500, pink.300, blue.500)"
              bgClip="text"
            >
              welcome to the TECK-TOWER-APP
            </Text>
          </Box>
          <Spacer />
          <Box mawW="250px"></Box>
          <Box maxW="200px" m="10">
            <Image
              boxSize="400px"
              objectFit="cover"
              src={towerimage}
              alt={"tower page"}
            ></Image>
          </Box>
          <Box maxW="400px">
            <Image
              boxSize="300px"
              objectFit="cover"
              src={radar}
              alt={"radar image"}
            ></Image>
          </Box>
        </Flex>
      </HStack>
    </>
  );
}

export default Home;
